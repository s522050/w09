import React, { Component } from 'react';
import TodoItems from './TodoItems';
import './Todo.css';


class TodoList extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            items: []
        };

        this.addItem = this.addItem.bind(this);
    }

    addItem(e) {

        var itemArray = this.state.items;
        
         if (this._inputElement.value !== "") {
           itemArray.unshift(
             {
               text: this._inputElement.value,
               key: Date.now()
             }
           );
        
           this.setState({
             items: itemArray
           });
        
           this._inputElement.value = "";
         }
        
         console.log(itemArray);
          
         e.preventDefault();
        }
    render() {
        return (
            <div className="todoListMain">
                <div className="header">
                    <header className="title">Welcome to ToDo List Application</header>
                    <form onSubmit={this.addItem}>
                        <input ref={(a) => this._inputElement = a} placeholder="Add ToDo List !"></input> <br>
                        </br>
                        <button type="submit">add</button>
                    </form>
                </div>
                <TodoItems entries={this.state.items} />
            </div>
        );
    }
};

export default TodoList;